package com.maildots.maildots;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.maildots.maildots.com.maildots.maildots.mail.MailItem;
import com.maildots.maildots.com.maildots.maildots.mainActivity.MainActivityAdapter;
import com.navdrawer.SimpleSideDrawer;

import java.util.ArrayList;
import java.util.List;

//Hola 4

public class MainActivity extends ActionBarActivity {

    private SimpleSideDrawer _navDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        List<MailItem> datos = new ArrayList<MailItem>();
        for (int i = 0; i < 200; i++)
            datos.add(new MailItem());

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.main_activity_recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new MainActivityAdapter(datos,this.getBaseContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        configurarDrawer();
    }

    private void configurarDrawer(){
        _navDrawer = new SimpleSideDrawer(this);
        _navDrawer.setLeftBehindContentView(R.layout.main_drawer_layout);
        _navDrawer.setAnimationDuration(300);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_ab_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.main_activity_menu_new_mail) {
            return true;
        }

        if (id == android.R.id.home){
            _navDrawer.toggleLeftDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
